#include <iostream>

template <typename T> class Stack {
private:
    T* _array = nullptr;
    int _length = 0;
    int _capacity;
public:

    explicit Stack(int initialCapacity) {
        _array = new T[initialCapacity]{};
        _capacity = initialCapacity;
    }

    /**
     * Добавление элемента в конец стека
     * @param a элемент
     */
    void push(T a) {
        _array[_length] = a;
        _length++;

        if (_length == _capacity) {
            // нужно выделить новый массив
            _increaseCapacity();
        }
    }

    /**
     * Получить элемент из конца стека, с удалением из стека
     * @return элемент из конца стека
     */
    T pop() {
        if (isEmpty()) {
            std::cout << "Trying to get element out of bounds";
            exit(-1);
        }
        _length--;
        return _array[_length];
    }

    /**
     * Просмотр элемента в конце стека
     * @return элемент из конца стека
     */
    T peek() {
        if (isEmpty()) {
            std::cout << "Trying to get element out of bounds";
            exit(-1);
        }
        return _array[_length - 1];
    }

    /**
     * Просмотр количества элементов в стеке
     * @return
     */
    int size() const {
        return _length;
    }

    /**
     * Проверка на отсутствие элементов
     * @return признак отсутствия элементов
     */
    bool isEmpty() const {
        return _length == 0;
    }

    /**
     * Освобождение ресурсов. Должен обязательно вызываться при завершении работы со стеком
     */
    void dispose() {
        delete[] _array;
        _array = nullptr;
    }

    /**
 * Увеличение размера встроенного массива
 */
private:
    void _increaseCapacity() {
        T* _temp_array = new T[_capacity * 2];
        for (int i = 0; i < _length; i++) {
            _temp_array[i] = _array[i];
        }
        delete[] _array;
        _array = _temp_array;
        _capacity *= 2;
    }
};

int main() {
    Stack<int> stack(2);

    stack.push(5);
    stack.push(7);
    stack.push(3);
    stack.push(9);
    stack.push(4);

    std::cout << stack.pop() << "\n";
    std::cout << stack.pop() << "\n";
    std::cout << stack.pop() << "\n";
    std::cout << stack.pop() << "\n";
    std::cout << stack.pop() << "\n";

    // Упадёт при попытке получить отсутствующий элемент
    std::cout << stack.pop() << "\n";

    stack.dispose();
    return 0;
}